FROM adoptopenjdk/openjdk11
MAINTAINER alexeyzhulin.ru
COPY target/ci-cd-demo-0.0.1.jar ci-cd-demo.jar
ENTRYPOINT ["java","-jar","/ci-cd-demo.jar"]
